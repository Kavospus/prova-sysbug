package tests;

import static org.junit.Assert.*;

import model.ClienteFisico;
import model.ClienteJuridico;
import model.ContaCorrente;
import model.ContaPoupanca;
import model.Funcionario;
import model.Presidente;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import controller.ControleBanco;

public class ControleBancoTest {
	ControleBanco controle;
	
	@Before
	public void setUp() throws Exception {
		controle = new ControleBanco();
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAdicionaConsultaClienteFisico() {
		ClienteFisico cliente = new ClienteFisico();
		cliente.setNome("Andre");
		cliente.setCpf("111.111.111-11");
		cliente.setTelefone("3333-4444");
		controle.adicionarCliente(cliente);
		
		assertEquals("Andre", controle.consultarCliente(0).getNome());
		assertEquals("3333-4444", controle.consultarCliente(0).getTelefone());
		assertEquals("111.111.111-11", ((ClienteFisico)controle.consultarCliente(0)).getCpf());
	}
	@Test
	public void testAdicionaConsultaClienteJuridico() {
		ClienteJuridico cliente = new ClienteJuridico();
		cliente.setNome("UnB");
		cliente.setCnpj("111.111.111-11");
		cliente.setTelefone("3333-4444");
		controle.adicionarCliente(cliente);
		
		assertEquals("UnB", controle.consultarCliente(0).getNome());
		assertEquals("3333-4444", controle.consultarCliente(0).getTelefone());
		assertEquals("111.111.111-11", ((ClienteJuridico)controle.consultarCliente(0)).getCnpj());
	}
	@Test
	public void testAdicionaConsultaFuncionario() {
		Presidente funcionario = new Presidente();
		funcionario.setNome("Andre");
		funcionario.setCpf("111.111.111-11");
		funcionario.setTelefone("3333-4444");
		funcionario.setSenhaDeAcesso(123);
		controle.adicionarFuncionario(funcionario);
		
		assertEquals("Andre", controle.consultarFuncionario(0).getNome());
		assertEquals("3333-4444", controle.consultarFuncionario(0).getTelefone());
		assertEquals("111.111.111-11", controle.consultarFuncionario(0).getCpf());
		assertEquals(123, ((Presidente)controle.consultarFuncionario(0)).getSenhaDeAcesso());
	}
	@Test
	public void testAdicionaConsultaConta() {
		ClienteFisico cliente = new ClienteFisico();
		ContaCorrente conta = new ContaCorrente();
		conta.setAgencia("123-9");
		conta.setNumero("2222-7");
		cliente.adicionarConta(conta);
		controle.adicionarCliente(cliente);
		assertEquals("123-9", ((ClienteFisico)controle.consultarCliente(0)).consultarConta(0).getAgencia());
		assertEquals("2222-7", ((ClienteFisico)controle.consultarCliente(0)).consultarConta(0).getNumero());
		assertEquals(1, ((ClienteFisico)controle.consultarCliente(0)).consultarTotalDeContas());
	}
	@Test
	public void testDepositaRetiraSaldo() {
		ClienteFisico cliente = new ClienteFisico();
		ContaCorrente conta = new ContaCorrente();
		conta.setSaldo(20.0);
		cliente.adicionarConta(conta);
		controle.adicionarCliente(cliente);
		assertEquals(20.0, ((ClienteFisico)controle.consultarCliente(0)).consultarConta(0).getSaldo(),0.01);
		((ClienteFisico)controle.consultarCliente(0)).consultarConta(0).adicionarFundos(20.0);
		assertEquals(40.0,((ClienteFisico)controle.consultarCliente(0)).consultarConta(0).getSaldo(),0.01);
		((ClienteFisico)controle.consultarCliente(0)).consultarConta(0).retirarFundos(30.0);
		assertEquals(10.0,((ClienteFisico)controle.consultarCliente(0)).consultarConta(0).getSaldo(),0.01);
	}
	@Test
	public void testCobrarManutencao() {
		ClienteFisico cliente = new ClienteFisico();
		ContaCorrente conta = new ContaCorrente();
		conta.setSaldo(20.0);
		conta.setValorManutencao(10.0);
		cliente.adicionarConta(conta);
		controle.adicionarCliente(cliente);
		((ContaCorrente)((ClienteFisico)controle.consultarCliente(0)).consultarConta(0)).cobraManutencao();
		assertEquals(10.0,((ClienteFisico)controle.consultarCliente(0)).consultarConta(0).getSaldo(),0.01);
	}
	@Test
	public void testContabilizaJuros() {
		ClienteFisico cliente = new ClienteFisico();
		ContaPoupanca conta = new ContaPoupanca();
		conta.setSaldo(20.0);
		conta.setTaxaJuros(10.0);
		cliente.adicionarConta(conta);
		controle.adicionarCliente(cliente);
		((ContaPoupanca)((ClienteFisico)controle.consultarCliente(0)).consultarConta(0)).contabilizaJuros();
		assertEquals(22.0,((ClienteFisico)controle.consultarCliente(0)).consultarConta(0).getSaldo(),0.01);
	}
	

}
