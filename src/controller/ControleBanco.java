package controller;

import java.util.ArrayList;
import model.Cliente;
import model.Conta;
import model.Diretor;
import model.Funcionario;
import model.Gerente;
import model.Presidente;

public class ControleBanco {

	private ArrayList<Cliente> clientes;
	private ArrayList<Funcionario> funcionarios;
	
	
	
	public ControleBanco() {
		clientes = new ArrayList<Cliente>();
		funcionarios = new ArrayList<Funcionario>();
	}
	
	public ArrayList<Cliente> getClientes() {
		return clientes;
	}
	public void setClientes(ArrayList<Cliente> clientes) {
		this.clientes = clientes;
	}
	public ArrayList<Funcionario> getFuncionarios() {
		return funcionarios;
	}
	public void setFuncionarios(ArrayList<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}
	public void adicionarFuncionario(Funcionario funcionario){
		this.funcionarios.add(funcionario);
	}
	public void adicionarCliente(Cliente cliente){
		this.clientes.add(cliente);
	}
	public Funcionario consultarFuncionario(int x){
		return this.funcionarios.get(x);
	}
	public Cliente consultarCliente(int x){
		return this.clientes.get(x);
	}
	public boolean removerFuncionario(Funcionario funcionario){
		if(this.funcionarios.contains(funcionario)){
			this.funcionarios.remove(funcionario);
			return true;
		}
		return false;
	}
	public boolean removerCliente(Cliente cliente){
		if(this.clientes.contains(cliente)){
			this.clientes.remove(cliente);
			return true;
		}
		return false;
	}
	
	
	
	
}
