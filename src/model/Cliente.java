package model;

import java.util.ArrayList;

public class Cliente extends Pessoa{
	private ArrayList<Conta> contas;

	public Cliente() {
		contas = new ArrayList<Conta>();
	}
	public ArrayList<Conta> getContas() {
		return contas;
	}
	public void setContas(ArrayList<Conta> contas) {
		this.contas = contas;
	}
	public void adicionarConta(Conta conta){
		this.contas.add(conta);
	}
	public Conta consultarConta(int x){
		return this.contas.get(x);
	}
	public int consultarTotalDeContas(){
		return this.contas.size();
	}
	public boolean removerConta(Conta conta){
		if(this.contas.contains(conta)){
			this.contas.remove(conta);
			return true;
		}
		return false;
	}
	
}
