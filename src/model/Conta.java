package model;

public class Conta {
	private String numero;
	private String agencia;
	private Double saldo;
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public void adicionarFundos(Double valor){
		this.setSaldo(this.getSaldo()+valor);
	}
	public void retirarFundos(Double valor){
		this.setSaldo(this.getSaldo()-valor);
	}
	
}
