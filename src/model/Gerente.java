package model;

public class Gerente extends Funcionario{
	private int senhaDeAcesso;
	private int numeroDeGerenciados;
	public int getSenhaDeAcesso() {
		return senhaDeAcesso;
	}
	public void setSenhaDeAcesso(int senhaDeAcesso) {
		this.senhaDeAcesso = senhaDeAcesso;
	}
	public int getNumeroDeGerenciados() {
		return numeroDeGerenciados;
	}
	public void setNumeroDeGerenciados(int numeroDeGerenciados) {
		this.numeroDeGerenciados = numeroDeGerenciados;
	}
	
}
